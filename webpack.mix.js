let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/backend/scss/AdminLTE.scss', 'public/assets/backend/css/admin_lte.css');
mix.copy('resources/assets/backend/img', 'public/assets/backend/images');
mix.copy('resources/assets/backend/plugins', 'public/assets/backend/plugins');

mix.scripts([
    'resources/assets/backend/js/admin_lte.js'
], 'public/assets/backend/js/admin_lte.js');
mix.copy('resources/assets/backend/js/custom/', 'public/assets/backend/js/custom/');
