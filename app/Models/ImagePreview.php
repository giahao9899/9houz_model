<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImagePreview extends Model
{
    protected $table = 'md_image_previews';
    public $timestamps = true;

    public function model() {
        return $this->belongsTo('App\Models\MdModel', 'model_id');
    }
}
