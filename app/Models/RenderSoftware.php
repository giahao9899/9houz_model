<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RenderSoftware extends Model
{
    protected $table = 'md_render_software';
    public $timestamps = true;

    public function models() {
        return $this->hasMany('App\Models\MdModel', 'render_software_id');
    }
}
