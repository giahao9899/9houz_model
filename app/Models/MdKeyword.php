<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MdKeyword extends Model
{
    protected $table = 'md_keywords';
    public $timestamps = true;

    public function models() {
        return $this->belongsToMany('App\Models\MdModel', 'model_keyword', 'keyword_id', 'model_id');
    }
}
