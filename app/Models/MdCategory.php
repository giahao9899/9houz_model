<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MdCategory extends Model
{
    protected $table = 'md_categories';
    public $timestamps = true;

    public function models() {
        return $this->hasMany('App\Models\MdModel', 'category_id');
    }

    public function parent() {
        return $this->belongsTo('App\Models\MdCategory', 'parent_id');
    }

    public function children() {
        return $this->hasMany('App\Models\MdCategory', 'parent_id');
    }

}
