<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MdModel extends Model
{
    protected $table = 'models';
    public $timestamps = true;

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function category() {
        return $this->belongsTo('App\Models\MdCategory', 'category_id');
    }

    public function imagePreviews() {
        return $this->hasMany('App\Models\MdModel', 'model_id');
    }

    public function drawSoftware() {
        return $this->belongsTo('App\Models\DrawSoftware', 'draw_software_id');
    }

    public function renderSoftware() {
        return $this->belongsTo('App\Models\RenderSoftware', 'render_software_id');
    }

    public function style() {
        return $this->belongsTo('App\Models\MdStyle', 'style_id');
    }

    public function keywords() {
        return $this->belongsToMany('App\Models\MdKeyword', 'model_keyword', 'model_id', 'keyword_id');
    }
}
