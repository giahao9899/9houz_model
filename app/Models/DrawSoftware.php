<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DrawSoftware extends Model
{
    protected $table = 'md_draw_software';
    public $timestamps = true;

    public function models() {
        return $this->hasMany('App\Models\MdModel', 'draw_software_id');
    }
}
