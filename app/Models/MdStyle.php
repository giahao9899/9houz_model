<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MdStyle extends Model
{
    protected $table = 'md_styles';
    public $timestamps = true;

    public function models() {
        return $this->hasMany('App\Models\MdModel', 'style_id');
    }
}
