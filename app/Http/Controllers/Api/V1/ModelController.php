<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\DrawSoftware;
use App\Models\MdCategory;
use App\Models\RenderSoftware;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModelController extends Controller
{

    public function getCategoryLv2(Request $request) {
        $id = $request->id;
        $category = MdCategory::where('parent_id', $id)->get();
        return response()->json($category);
    }

    public function getDrawsSoftwareVersion(Request $request) {
        $name = $request->name;
        $draws_software = DrawSoftware::where('name', $name)->get();
        return response()->json($draws_software);
    }

    public function getRendersSoftwareVersion(Request $request) {
        $name = $request->name;
        $renders_software = RenderSoftware::where('name', $name)->get();
        return response()->json($renders_software);
    }
}
