<?php

namespace App\Http\Controllers\Backend;

use App\Models\DrawSoftware;
use App\Models\ImagePreview;
use App\Models\MdCategory;
use App\Models\MdModel as Model;
use App\Models\MdStyle;
use App\Models\RenderSoftware;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ModelController extends Controller
{

    public function index() {
        $models = Model::where('active', 1)->paginate(25);
        return view('backend.model.index', compact([
            'models'
        ]));
    }

    public function create() {
        $categories = MdCategory::where('status', 1)->get();
        $draws_software = DrawSoftware::groupBy('name')->selectRaw('count(*) as total, name')->get();
        $renders_software = RenderSoftware::groupBy('name')->selectRaw('count(*) as total, name')->get();
        $styles = MdStyle::all();

        return view('backend.model.create', compact([
            'categories',
            'draws_software',
            'renders_software',
            'styles'
        ]));
    }

    public function store(Request $request) {

        $request->validate([
            'category_lv2' => 'required',
            'title' => 'required',
            'desc' => 'required',
            'draw_software' => 'required',
            'render_software' => 'required',
            'style' => 'required',
            'image_preview' => 'required',
            'image_preview.*' => 'image'
        ]);


        if($request->hasfile('image_preview'))
        {
            $model = new Model();
            $model->category_id = $request->category_lv2;
            $model->title = $request->title;
            $model->slug = Str::slug($model->title);
            $model->desc = $request->desc;
            $model->draw_software_id = $request->draw_software;
            $model->render_software_id = $request->render_software;
            $model->style_id = $request->style;
            $model->user_id = Auth::user()->id;
            $model->active = 1;

            //Lưu file model tải lên ???

            $model->path_file = 'lorem';

            $model->save();

            foreach($request->file('image_preview') as $file)
            {
                $path = $file->store('image_model');
                $image_preview = new ImagePreview();
                $image_preview->path = $path;
                $image_preview->model_id = $model->id;
                $image_preview->save();
            }
        }

        return redirect()->route('admin.model.index');
    }
}
