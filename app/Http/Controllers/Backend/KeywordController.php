<?php

namespace App\Http\Controllers\Backend;

use App\Models\MdKeyword;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KeywordController extends Controller
{
    public function index() {
        $keywords = MdKeyword::orderBy('created_at', 'desc')->paginate(25);
        return view('backend.keyword.index', compact([
            'keywords'
        ]));
    }

    public function store(Request $request) {
        $request->validate([
            'keyword' => 'required|unique:md_keywords,name|min:6|max:45'
        ]);

        $keyword = new MdKeyword();
        $keyword->name = $request->keyword;
        $keyword->save();

        return redirect()->back();
    }
}
