<?php

namespace App\Http\Controllers\Backend;

use App\Models\MdCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{

    public function index() {
        $categories = MdCategory::where('status', 1)->get();
        return view('backend.category.index', compact([
            'categories'
        ]));
    }
}
