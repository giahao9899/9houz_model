@extends('backend.layouts.master')

@section('after-script-end')

@endsection

@section('content_header')
    Category
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools">
                            <span>Cấp 1</span>
                            <form action="" class="form-inline">
                                <div class="form-group">
                                    <input type="text" class="form-control">
                                    <button type="submit" class="btn btn-primary ml-3">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <table class="table table-responsive" id="myTable">
                            <thead>
                                <th>#</th>
                                <th>Title</th>
                            </thead>
                            <tbody>
                                @foreach($categories as $category)
                                    <tr class="clickable-row">
                                        <td>{{$category->id}}</td>
                                        <td>{{$category->title}}</td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="card-tools">

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools">
                            <span>Cấp 2</span>
                            <form action="" class="form-inline">
                                <div class="form-group">
                                    <input type="text" class="form-control">
                                    <button type="submit" class="btn btn-primary ml-3">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <table class="table table-responsive" id="tableCate2">
                            <thead>
                            <th>#</th>
                            <th>Title</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="card-tools">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!--/. container-fluid -->
@endsection

@section('end-script')
    <script>
        $(document).ready(function() {
            $('#myTable').on('click','.clickable-row', function (event) {
                if($(this).hasClass('table-primary')){
                    $(this).removeClass('table-primary');
                } else {
                    let categoryId = $(this).children('td:nth-child(1)').text();
                    $(this).addClass('table-primary').siblings().removeClass('table-primary');
                    $.get("/api/v1/getCategoryLv2?id=" + categoryId, function(categories){
                        let result = '';
                        categories.forEach(function (category) {
                            result += `<tr><td>${category.id}</td><td>${category.title}</td></tr>`;
                        });
                        $('#tableCate2 tbody').html(result);
                    });
                }
            });
        })
    </script>
    @endsection
