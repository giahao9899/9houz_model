@extends('backend.layouts.master')

@section('after-script-end')

@endsection

@section('content_header')
    Dashboard
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools">
                            <form action="{{route('admin.keyword.store')}}" method="POST" class="form-inline">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control" name="keyword">
                                    <button type="submit" class="btn btn-primary ml-3">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <table class="table table-responsive">
                            <thead>
                            <th>#</th>
                            <th>Keyword</th>
                            </thead>
                            <tbody>
                            @foreach($keywords as $keyword)
                                <tr>
                                    <td>{{$keyword->id}}</td>
                                    <td>{{$keyword->name}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="card-tools">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!--/. container-fluid -->
@endsection
