@extends('backend.layouts.master')

@section('after-script-end')

@endsection

@section('content_header')
    Model
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools">
                            {{ $models->links("pagination::bootstrap-4") }}
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <table class="table table-responsive">
                            <thead>
                                <th>#</th>
                                <th>Tiêu đề</th>
                                <th>Danh mục</th>
                                <th>Phần mềm vẽ</th>
                                <th>Phần mềm render</th>
                                <th>Phong các</th>
                                <th>Từ khóa</th>
                                <th>User</th>
                                <th>Active</th>
                            </thead>
                            <tbody>
                                @foreach($models as $model)
                                    <tr>
                                        <td>{{$model->id}}</td>
                                        <td>{{$model->title}}</td>
                                        <td>{{$model->category->title}}</td>
                                        <td>{{$model->drawSoftware->name}} - {{$model->drawSoftware->version}}</td>
                                        <td>{{$model->renderSoftware->name}} - {{$model->renderSoftware->version}}</td>
                                        <td>{{$model->style->name}}</td>
                                        <td></td>
                                        <td>{{$model->user_id}}</td>
                                        <td></td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="card-tools">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!--/. container-fluid -->
@endsection
