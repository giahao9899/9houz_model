@extends('backend.layouts.master')

@section('after-script-end')

@endsection

@section('content_header')
    Model
@endsection

@section('content')
    <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Model</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" method="post" enctype="multipart/form-data" action="{{route('admin.model.store')}}">
                    @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Ảnh Preview</label>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="file" multiple name="image_preview[]" id="image_preview">
                                        </div>
                                        <div class="col-12" id="display_image">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Danh mục</label>
                                <div class="col-md-5">
                                    <select required class="form-control" name="category_lv1" id="category_lv1">
                                        <option disabled selected>--- Danh mục cấp 1 ---</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select required class="form-control" name="category_lv2" id="category_lv2">
                                        <option disabled selected>--- Danh mục cấp 2 ---</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Tiêu đề Model</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="title">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Mô tả Model</label>
                                <div class="col-md-10">
                                    <textarea name="desc" id="" rows="5" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Phần mềm vẽ</label>
                                <div class="col-md-5">
                                    <select required class="form-control" id="draw_software">
                                        <option disabled selected>--- Phần mềm vẽ ---</option>
                                        @foreach($draws_software as $draw_software)
                                            <option value="{{$draw_software->name}}">{{$draw_software->name}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select required class="form-control"  name="draw_software"  id="draw_software_version">
                                        <option disabled selected>-- Version --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Phần mềm render</label>
                                <div class="col-md-5">
                                    <select required class="form-control" id="render_software">
                                        <option disabled selected>--- Phần mềm render ---</option>
                                        @foreach($renders_software as $render_software)
                                            <option value="{{$render_software->name}}">{{$render_software->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select required class="form-control" name="render_software" id="render_software_version">
                                        <option disabled selected>-- Version --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Phong cách</label>
                                <div class="col-md-10">
                                    <select required class="form-control" name="style">
                                        <option disabled selected>--- Phong cách ---</option>
                                        @foreach($styles as $style)
                                            <option value="{{$style->id}}">{{$style->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Từ khóa</label>

                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Chọn file</label>
                                <div class="input-group col-md-10">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="fileModel">
                                        <label class="custom-file-label" for="fileModel">Choose file</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="">Upload</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">Hoặc link Google driver</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control">
                                </div>

                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary px-5">Lưu</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!--/. container-fluid -->
@endsection

@section('end-script')
    <script>
        $(document).ready(function () {
            $('#category_lv1').change(function() {
                let categoryId = $(this).val();
                $.get("/api/v1/getCategoryLv2?id=" + categoryId, function(categories){
                    let result = '';
                    categories.forEach(function (category) {
                        result += `<option value="${category.id}">${category.title}</option>`;
                    });
                    $('#category_lv2').html(result);
                });

            });

            $('#draw_software').change(function() {
                let name = $(this).val();
                $.get("/api/v1/getDrawsSoftwareVersion?name=" + name, function(data){
                    let result = '';
                    data.forEach(function (drawSw) {
                        result += `<option value="${drawSw.id}">${drawSw.version}</option>`;
                    });
                    $('#draw_software_version').html(result);
                });

            });

            $('#render_software').change(function() {
                let name = $(this).val();
                $.get("/api/v1/getRendersSoftwareVersion?name=" + name, function(data){
                    let result = '';
                    data.forEach(function (renderSw) {
                        result += `<option value="${renderSw.id}">${renderSw.version}</option>`;
                    });
                    $('#render_software_version').html(result);
                });

            });

            // Multiple images preview in browser
            var imagesPreview = function(input, placeToInsertImagePreview) {

                if (input.files) {
                    var filesAmount = input.files.length;

                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();

                        reader.onload = function(event) {
                            $($.parseHTML('<img class="img-fluid d-inline-block" style="max-width: 100px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                        }

                        reader.readAsDataURL(input.files[i]);
                    }
                }

            };

            $('#image_preview').on('change', function() {
                imagesPreview(this, '#display_image');
            });
        });

    </script>
@endsection
