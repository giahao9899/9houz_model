<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Backend'], function () {

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Auth\LoginController@login');


    Route::group(['middleware' => 'auth'], function () {
        Route::get('model', 'ModelController@index')->name('admin.model.index');
        Route::get('model/create', 'ModelController@create')->name('admin.model.create');
        Route::post('model/create', 'ModelController@store')->name('admin.model.store');

        Route::get('category', 'CategoryController@index')->name('admin.category.index');

        Route::get('keyword', 'KeywordController@index')->name('admin.keyword.index');
        Route::post('keyword/create', 'KeywordController@store')->name('admin.keyword.store');
    });

});
